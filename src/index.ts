import { error } from 'util';
import * as request from 'request';
import * as console from 'console';

import { RemoteCamera } from './remote-camera';

async function moveMotor(motorUrl: string, N: number): Promise<any> {
  const delay = 6, steps = 512 / N;
  return new Promise((resolve, reject) => {
    return request(motorUrl + '/forward?delay=' + delay + '&steps=' + steps, (err) => {
      if (err) reject();
      else resolve();
    });
  });
}

async function main() {
  let camera = new RemoteCamera('http://192.168.122.1:8080/sony');

  try {
    await camera.init();

    const motorUrl = 'http://192.168.137.112:8080';
    const N = 16;
    const pictures = new Array(N);
    const promises = [];

    const BASE_NAME = 'pictures/picture_'

    for (let i = 0; i < N; i++) {
      await camera.waitForIdle();
      let postview = await camera.takePicture();
      pictures[i] = postview;

      let success = false;
      while (!success) {
        try {
          await camera.download(postview, BASE_NAME + i + ".jpg");
          success = true;
        } catch (error) {
          console.error(error);
          success = false;
        }
      }
      await moveMotor(motorUrl, N);
    }

    console.log(pictures);

    await camera.stop();
    return Promise.all(promises);
  } catch (e) {
    console.error(e);
  }
}

main();