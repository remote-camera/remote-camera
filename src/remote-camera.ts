import fs = require('fs');
import request = require('request');
import ahr2 = require('ahr2');

export class RemoteCamera {

  url: string;
  apiList: string[];

  constructor(host: string) {
    this.url = host + '/camera';
  }

  private execute(method: string, params = []): Promise<any[]> {
    const body = {
      method: method,
      version: "1.0",
      id: 1,
      params: params
    };
    return new Promise((resolve, reject) => {
      request.post(this.url, {
        json: true,
        body: body
      }, function (err, res, body) {
        if (err) {
          reject(err);
        } else {
          if (body.error) {
            reject(body.error);
          } else {
            resolve(body.result);
          }
        }
      });
    });
  }

  async init(): Promise<boolean> {
    this.apiList = (await this.execute('getAvailableApiList'))[0];
    if (this.apiList.indexOf('startRecMode') > 0) {
      await this.execute('startRecMode');
    }
    this.apiList = (await this.execute('getAvailableApiList'))[0];
    return this.apiList.indexOf('actTakePicture') > 0;
  }

  async stop(): Promise<any> {
    if (this.apiList.indexOf('stopRecMode') > 0) {
      return await this.execute('stopRecMode');
    }
  }

  async takePicture(): Promise<string> {
    return (await this.execute('actTakePicture'))[0][0];
  }

  async waitForIdle() {
    while ((await this.execute('getEvent', [false]))[1].cameraStatus !== 'IDLE');
  }

  download(uri, filename): Promise<any> {
    const options = {
      url: uri,
      method: 'GET'
    };
    return new Promise((resolve, reject) =>
      request(options)
        .on('error', reject)
        .pipe(fs.createWriteStream(filename))
        .on('close', resolve)
    );
  };

  download2(url, filename): Promise<any> {
    return new Promise((resolve, reject) =>
      ahr2.get(url).when(function (err, ahr, data) {
        fs.writeFile(filename, data, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      })
    );
  }
}